from pandapower import pandapowerNet

from pfasdr.grid_pandapower.model.get_mv_oberrhein_net_empty import \
    get_mv_oberrhein_net_empty
from pfasdr.grid_pandapower.model.get_mv_oberrhein_net_preset import \
    get_mv_oberrhein_net_preset


def get_grid_names_to_networks():
    grid_names_to_networks : dict = {}

    grid_name_mv_oberrhein: str = 'mv_oberrhein_empty'
    network_mv_oberrhein: pandapowerNet = get_mv_oberrhein_net_empty()
    grid_names_to_networks[grid_name_mv_oberrhein] = network_mv_oberrhein

    grid_name_mv_oberrhein_preset: str = 'mv_oberrhein_preset'
    network_mv_oberrhein_preset: pandapowerNet = get_mv_oberrhein_net_preset()
    grid_names_to_networks[grid_name_mv_oberrhein_preset] = \
        network_mv_oberrhein_preset

    return grid_names_to_networks
