from pathlib import Path

ROOT_PATH: Path = Path(__file__).parent.parent.parent.parent
DATA_GRID_MODELS_PATH: Path = ROOT_PATH / 'data' / 'grid' / 'models'
