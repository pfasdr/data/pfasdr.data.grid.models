import pickle
import platform

import pandapower

from pfasdr_grid_models.grid_models.get_grid_names_to_networks_module import \
    get_grid_names_to_networks
from pfasdr_grid_models.grid_models.util.paths import DATA_GRID_MODELS_PATH


def test_store_and_reload_grids():
    # Mock
    grid_names_to_networks = get_grid_names_to_networks()

    for grid_name, network in grid_names_to_networks.items():
        # Test
        filename_p = DATA_GRID_MODELS_PATH / f'{grid_name}.p'
        pandapower.to_pickle(net=network, filename=str(filename_p))
        network_p = pandapower.from_pickle(filename=filename_p)

        # Assert
        for dataframe_net, dataframe_p in zip(network, network_p):
            if type(dataframe_net) == str:
                assert dataframe_net == dataframe_p
                continue
            assert dataframe_net.equals(dataframe_p)

        # Test
        filename_excel = DATA_GRID_MODELS_PATH / f'{grid_name}.xlsx'
        pandapower.to_excel(net=network, filename=filename_excel)
        network_excel = pandapower.from_excel(filename=filename_excel)

        # Assert
        for dataframe_net, dataframe_excel in zip(network, network_excel):
            if type(dataframe_net) == str:
                assert dataframe_net == dataframe_excel
                continue
            assert dataframe_net.equals(dataframe_excel)

        # Test
        filename_json = DATA_GRID_MODELS_PATH / f'{grid_name}.json'
        pandapower.to_json(net=network, filename=filename_json)
        network_json = pandapower.from_json(filename=filename_json)

        # Assert
        if platform.python_implementation() == 'PyPy':
            network, network_json = sorted(network), sorted(network_json)

        for dataframe_net, dataframe_json in \
                zip(network, network_json):
            if type(dataframe_net) == str:
                assert dataframe_net == dataframe_json
                continue
            assert dataframe_net.equals(dataframe_json)

        # Test
        filename_pickle = DATA_GRID_MODELS_PATH / f'{grid_name}.pickle'
        with open(file=filename_pickle, mode='wb') as pickle_file:
            pickle.dump(obj=network, file=pickle_file,
                        protocol=pickle.HIGHEST_PROTOCOL)
        with open(file=filename_pickle, mode='rb') as pickle_file:
            network_pickle = pickle.load(file=pickle_file)

        # Assert
        for dataframe_net, dataframe_p in zip(network, network_pickle):
            if type(dataframe_net) == str:
                assert dataframe_net == dataframe_p
                continue
            assert dataframe_net.equals(dataframe_p)

        # Somehow, exporting as SQL does not work under PyPy.
        # So skip it.
        # TODO Enable this once it works under PyPy.
        if platform.python_implementation() == 'PyPy':
            return

        # Test
        filename_sql = DATA_GRID_MODELS_PATH / f'{grid_name}.sql'
        pandapower.to_sqlite(net=network, filename=str(filename_sql))
        network_sql = pandapower.from_sqlite(filename=str(filename_sql))

        # Assert
        for dataframe_net, dataframe_sql in zip(network, network_sql):
            if type(dataframe_net) == str:
                assert dataframe_net == dataframe_sql
                continue
            assert dataframe_net.equals(dataframe_sql)
