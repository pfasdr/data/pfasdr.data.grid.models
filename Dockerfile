FROM ubuntu:20.04

MAINTAINER Bengt Lüers "bengt.lueers@gmail.com"

RUN \
    apt update && \
    apt install --yes --quiet --quiet --no-install-recommends \
        software-properties-common && \
    add-apt-repository --yes ppa:deadsnakes/ppa && \
    apt update && \
    apt install --yes --quiet --quiet \
        python3.8-venv \
        python3.8 \
        python3-pip \
        git

COPY . /app
WORKDIR /app

RUN python3.8 -m venv venv
RUN venv/bin/python3.8 -m pip install --no-cache-dir -r requirements.d/venv.txt

RUN venv/bin/python3.8 -m tox -e py38 --notest

ENV PYTHONPATH=.
CMD .tox/py38/bin/python ./pfasdr_grid_models/grid_models/util/paths.py
