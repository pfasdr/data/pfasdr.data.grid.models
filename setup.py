import glob

from setuptools import setup, find_namespace_packages

NAMESPACE = 'pfasdr'
PACKAGE = 'data_grid_models'


def get_data_files():
    data_files = []
    directories = glob.glob('data/grid/')
    for directory in directories:
        files = glob.glob(directory + '*')
        data_files.append((directory, files))

    print(data_files)


setup(
    author='Bengt Lüers',
    author_email='bengt.lueers@gmail.com',
    description='Grid models created by PFASDR.Grid-Modeller',
    entry_points={
    },
    include_package_data=True,
    install_requires=[
    ],
    long_description=(
        open('README.md').read()
    ),
    long_description_content_type='text/markdown',
    name=NAMESPACE + '.' + PACKAGE,
    packages=find_namespace_packages(include=[NAMESPACE + '.*']),
    package_dir={'': '.'},
    data_files=get_data_files(),
    setup_requires=[
    ],
    tests_require=[
    ],
    url='https://gitlab.com/pfasdr/grid/pfasdr.data.grid.models',
    version='0.0.0',
    zip_safe=False,
)
